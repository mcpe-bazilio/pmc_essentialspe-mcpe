<?php
namespace EssentialsPE\Commands;

use EssentialsPE\BaseFiles\BaseAPI;
use EssentialsPE\BaseFiles\BaseCommand;
use pocketmine\command\CommandSender;
use pocketmine\Player;
use pocketmine\utils\TextFormat;

class ClearInventory extends BaseCommand {
	/**
	 * @param BaseAPI $api
	 */
	public function __construct(BaseAPI $api){
		parent::__construct($api, "clearinventory", "Отчистить Ваш/чужой инвентарь", "[игрок]", true, ["ci", "clean", "clearinvent"]);
		$this->setPermission("essentials.clearinventory.use");
	}

	/**
	 * @param CommandSender $sender
	 * @param string        $alias
	 * @param array         $args
	 *
	 * @return bool
	 */
	public function execute(CommandSender $sender, $alias, array $args): bool{
		if(!$this->testPermission($sender)){
			return false;
		}
		if((!isset($args[0]) && !$sender instanceof Player) || count($args) > 1){
			$this->sendUsage($sender, $alias);
			return false;
		}
		$player = $sender;
		if(isset($args[0])){
			if(!$sender->hasPermission("essentials.clearinventory.other")){
				$sender->sendMessage(TextFormat::RED . "[✘] " . $this->getPermissionMessage());
				return false;
			}elseif(!($player = $this->getAPI()->getPlayer($args[0]))){
				$sender->sendMessage(TextFormat::RED . "[✘] Игрок не найден");
				return false;
			}
		}
		if(($gm = $player->getGamemode()) === 1 || $gm === 3){
			$sender->sendMessage(TextFormat::RED . "[✘] " . (isset($args[0]) ? "Игрок " . $player->getDisplayName() : "Вы") . " в режиме " . $this->getAPI()->getServer()->getGamemodeString($gm));
			return false;
		}
		$player->getInventory()->clearAll();
		$player->sendMessage(TextFormat::AQUA . "Ваш инвентарь очищен");
		if($player !== $sender){
			$sender->sendMessage(TextFormat::AQUA . (isset($args[0]) ? "[✔] Инвентарь игрока " . $player->getDisplayName() : "[✔] Ваш инвентарь ") . "очищен");
		}
		return true;
	}
}
