<?php
namespace EssentialsPE\Commands\Warp;

use EssentialsPE\BaseFiles\BaseAPI;
use EssentialsPE\BaseFiles\BaseCommand;
use pocketmine\command\CommandSender;
use pocketmine\Player;
use pocketmine\utils\TextFormat;

class Setwarp extends BaseCommand{
    /**
     * @param BaseAPI $api
     */
    public function __construct(BaseAPI $api){
        parent::__construct($api, "setwarp", "Установить или обновить варп", "<имя_варпа>", false, ["openwarp", "createwarp"]);
        $this->setPermission("essentials.setwarp");
    }

    /**
     * @param CommandSender $sender
     * @param string $alias
     * @param array $args
     * @return bool
     */
    public function execute(CommandSender $sender, $alias, array $args): bool{
        if(!$this->testPermission($sender)){
            return false;
        }
        if(!$sender instanceof Player || count($args) !== 1){
            $this->sendUsage($sender, $alias);
            return false;
        }
        if(($existed = $this->getAPI()->warpExists($args[0])) && !$sender->hasPermission("essentials.warp.override.*") && !$sender->hasPermission("essentials.warp.override.$args[0]")){
            $sender->sendMessage(TextFormat::RED . "[✘] У Вас нет прав на изменение места варпа");
            return false;
        }
        if(!$this->getAPI()->setWarp($args[0], $sender->getPosition(), $sender->getYaw(), $sender->getPitch())){
            $sender->sendMessage(TextFormat::RED . "[✘] Неверное имя варпа! Используйте буквы и символ подчеркивания");
            return false;
        }
        $sender->sendMessage(TextFormat::GREEN . "[✔] Варп " . ($existed ? "обновлен!" : "создан!"));
        return true;
    }
} 