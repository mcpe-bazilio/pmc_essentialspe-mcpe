<?php
namespace EssentialsPE\Commands\Warp;

use EssentialsPE\BaseFiles\BaseAPI;
use EssentialsPE\BaseFiles\BaseCommand;
use pocketmine\command\CommandSender;
use pocketmine\Player;
use pocketmine\utils\TextFormat;

class Warp extends BaseCommand{
    /**
     * @param BaseAPI $api
     */
    public function __construct(BaseAPI $api){
        parent::__construct($api, "warp", "Телепортация на варп", "[[имя_варпа] [игрок]]", true, ["warps"]);
        $this->setPermission("essentials.warp.use");
    }

    /**
     * @param CommandSender $sender
     * @param string $alias
     * @param array $args
     * @return bool
     */
    public function execute(CommandSender $sender, $alias, array $args): bool{
        if(!$this->testPermission($sender)){
            return false;
        }
        if(count($args) === 0){
            if(($list = $this->getAPI()->warpList(false)) === false){
                $sender->sendMessage(TextFormat::AQUA . "Сейчас нет доступных варпов.");
                return false;
            }
            $sender->sendMessage(TextFormat::AQUA . "Доступные варпы:\n" . $list);
            return true;
        }
        if(!($warp = $this->getAPI()->getWarp($args[0]))){
            $sender->sendMessage(TextFormat::RED . "[✘] Варпа " . $args[0] . "не существует");
            return false;
        }
        if(!isset($args[1]) && !$sender instanceof Player){
            $this->sendUsage($sender, $alias);
            return false;
        }
        $player = $sender;
        if(isset($args[1])){
            if(!$sender->hasPermission("essentials.warp.other")){
                $sender->sendMessage(TextFormat::RED . "[✘] У Вас нет прав телепортировать других игроков");
                return false;
            }elseif(!($player = $this->getAPI()->getPlayer($args[1]))){
                $sender->sendMessage(TextFormat::RED . "[✘] Игрок не найден");
                return false;
            }
        }
        if(!$sender->hasPermission("essentials.warps.*") && !$sender->hasPermission("essentials.warps.$args[0]")){
            $sender->sendMessage(TextFormat::RED . "[✘] Вы не можете никого телепортировать на этот варп");
            return false;
        }
        $player->teleport($warp);
        $player->sendMessage(TextFormat::GREEN . "[✔] Вы телепортируетесь на варп " . TextFormat::AQUA . $warp->getName() . TextFormat::GREEN . "...");
        if($player !== $sender){
            $sender->sendMessage(TextFormat::GREEN . "[✔] Телепортирую " . TextFormat::YELLOW . $player->getDisplayName() . TextFormat::GREEN . " на варп " . TextFormat::AQUA . $warp->getName() . TextFormat::GREEN . "...");
        }
        return true;
    }
} 
