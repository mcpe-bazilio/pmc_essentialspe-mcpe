<?php
namespace EssentialsPE\Commands\Home;

use EssentialsPE\BaseFiles\BaseAPI;
use EssentialsPE\BaseFiles\BaseCommand;
use pocketmine\command\CommandSender;
use pocketmine\Player;
use pocketmine\utils\TextFormat;

class SetHome extends BaseCommand{
    /**
     * @param BaseAPI $api
     */
    public function __construct(BaseAPI $api){
        parent::__construct($api, "sethome", "Установить или обновить точку ДОМА", "<имя_ДОМА>", false, ["createhome"]);
        $this->setPermission("essentials.sethome");
    }

    /**
     * @param CommandSender $sender
     * @param string $alias
     * @param array $args
     * @return bool
     */
    public function execute(CommandSender $sender, $alias, array $args): bool{
        if(!$this->testPermission($sender)){
            return false;
        }
        if(!$sender instanceof Player || count($args) !== 1){
            $this->sendUsage($sender, $alias);
            return false;
        }
        if(strtolower($args[0]) === "bed"){
            $sender->sendMessage(TextFormat::RED . "[✘] ДОМ с именем \"bed\" устанавливается автоматически, когда вы спите на кровати");
            return false;
        }elseif(trim($args[0] === "")){
            $sender->sendMessage(TextFormat::RED . "[✘] Укажите название ДОМА");
            return false;
        }
        $homeExists = $this->getAPI()->homeExists($sender, $args[0]);
        if(!$this->getAPI()->setHome($sender, strtolower($args[0]), $sender->getLocation(), $sender->getYaw(), $sender->getPitch())){
            $sender->sendMessage(TextFormat::RED . "[✘] Имя ДОМА не соответствует правилам. Используйте только буквы и символ подчеркивания");
            return false;
        }
        $sender->sendMessage(TextFormat::GREEN . "[✔] Точка ДОМА " . ($homeExists ? "обновлена" : "создана"));
        return true;
    }
} 
