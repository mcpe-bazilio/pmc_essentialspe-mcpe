<?php
namespace EssentialsPE\Commands\Home;

use EssentialsPE\BaseFiles\BaseAPI;
use EssentialsPE\BaseFiles\BaseCommand;
use pocketmine\command\CommandSender;
use pocketmine\Player;
use pocketmine\utils\TextFormat;

class Home extends BaseCommand{
    /**
     * @param BaseAPI $api
     */
    public function __construct(BaseAPI $api){
        parent::__construct($api, "home", "Телепортация к ДОМУ", "<имя_ДОМА>", false, ["homes"]);
        $this->setPermission("essentials.home.use");
    }

    /**
     * @param CommandSender $sender
     * @param string $alias
     * @param array $args
     * @return bool
     */
    public function execute(CommandSender $sender, $alias, array $args): bool{
        if(!$this->testPermission($sender)){
            return false;
        }
        if(!$sender instanceof Player || count($args) > 1){
            $this->sendUsage($sender, $alias);
            return false;
        }
        if(count($args) === 0){
            if(($list = $this->getAPI()->homesList($sender, false)) === false){
                $sender->sendMessage(TextFormat::AQUA . "[✘] У вас пока нет ДОМОВ");
                return false;
            }
            $sender->sendMessage(TextFormat::AQUA . "Ваши точки дома:\n" . $list);
            return true;
        }
        if(!($home = $this->getAPI()->getHome($sender, $args[0]))){
            $sender->sendMessage(TextFormat::RED . "[✘] Такого ДОМА не существует или не доступен мир, где он находится");
            return false;
        }
        $sender->teleport($home);
        $sender->sendMessage(TextFormat::GREEN . "[✔] Телепортирую к ДОМУ " . TextFormat::AQUA . $home->getName() . TextFormat::GREEN . "...");
        return true;
    }
} 