<?php
namespace EssentialsPE\Commands\PowerTool;

use EssentialsPE\BaseFiles\BaseAPI;
use EssentialsPE\BaseFiles\BaseCommand;
use pocketmine\command\CommandSender;
use pocketmine\item\Item;
use pocketmine\Player;
use pocketmine\utils\TextFormat;

class PowerTool extends BaseCommand{
    /**
     * @param BaseAPI $api
     */
    public function __construct(BaseAPI $api){
        parent::__construct($api, "powertool", "Включение PowerTool - назначение команд и чат-макросов предмету, который сейчас в руках", "<command|c:chat macro> <arguments...>", false, ["pt"]);
        $this->setPermission("essentials.powertool.use");
    }

    /**
     * @param CommandSender $sender
     * @param string $alias
     * @param array $args
     * @return bool
     */
    public function execute(CommandSender $sender, $alias, array $args): bool{
        if(!$this->testPermission($sender)){
            return false;
        }
        if(!$sender instanceof Player){
            $this->sendUsage($sender, $alias);
            return false;
        }
        $item = $sender->getInventory()->getItemInHand();
        if($item->getId() === Item::AIR){
            $sender->sendMessage(TextFormat::RED . "[✘] Вы не можете назначить команду, если у вс в руках ничего нет.");
            return false;
        }

        if(count($args) === 0){
            if(!$this->getAPI()->getPowerToolItemCommand($sender, $item) && !$this->getAPI()->getPowerToolItemCommands($sender, $item) && !$this->getAPI()->getPowerToolItemChatMacro($sender, $item)){
                $this->sendUsage($sender, $alias);
                return false;
            }
            if($this->getAPI()->getPowerToolItemCommand($sender, $item) !== false){
                $sender->sendMessage(TextFormat::GREEN . "[✔] Команда отключена от этого предмета.");
            }elseif($this->getAPI()->getPowerToolItemCommands($sender, $item) !== false){
                $sender->sendMessage(TextFormat::GREEN . "[✔] Команды отключены от этого предмета.");
            }
            if($this->getAPI()->getPowerToolItemChatMacro($sender, $item) !== false){
                $sender->sendMessage(TextFormat::GREEN . "[✔] Чат-макрос отключен от этого предмета.");
            }
            $this->getAPI()->disablePowerToolItem($sender, $item);
        }else{
            if($args[0] === "pt" || $args[0] === "ptt" || $args[0] === "powertool" || $args[0] === "powertooltoggle"){
                $sender->sendMessage(TextFormat::RED . "[✘] Эта команду назначать нельзя");
                return false;
            }
            $command = implode(" ", $args);
            if(stripos($command, "c:") !== false){ //Create a chat macro
                $c = substr($command, 2);
                $this->getAPI()->setPowerToolItemChatMacro($sender, $item, $c);
                $sender->sendMessage(TextFormat::GREEN . "[✔] Чат-макрос назначен предмету в руках (" . $item->getName(). ")");
            }elseif(stripos($command, "a:") !== false){
                if(!$sender->hasPermission("essentials.powertool.append")){
                    $sender->sendMessage(TextFormat::RED . "[✘] ". $this->getPermissionMessage());
                    return false;
                }
                $commands = substr($command, 2);
                $commands = explode(";", $commands);
                $this->getAPI()->setPowerToolItemCommands($sender, $item, $commands);
                $sender->sendMessage(TextFormat::GREEN . "[✔] Команды назначены предмету в руках (" . $item->getName(). ")");
            }elseif(stripos($command, "r:") !== false){
                if(!$sender->hasPermission("essentials.powertool.append")){
                    $sender->sendMessage(TextFormat::RED . "[✘] ". $this->getPermissionMessage());
                    return false;
                }
                $command = substr($command, 2);
                $this->getAPI()->removePowerToolItemCommand($sender, $item, $command);
                $sender->sendMessage(TextFormat::GREEN . "[✔] Команда отключена от предмета в руках (" . $item->getName(). ")");
            }elseif(count($args) === 1 && (($a = strtolower($args[0])) === "l" || $a === "d")){
                switch($a){
                    case "l":
                        $commands = false;
                        if($this->getAPI()->getPowerToolItemCommand($sender, $item) !== false){
                            $commands = $this->getAPI()->getPowerToolItemCommand($sender, $item);
                        }elseif($this->getAPI()->getPowerToolItemCommands($sender, $item) !== false){
                            $commands = $this->getAPI()->getPowerToolItemCommand($sender, $item);
                        }
                        $list = "=== Command ===";
                        if($commands === false){
                            $list .= "\n" . TextFormat::ITALIC . "**Предмету (" . $item->getName(). ") не назначено команд**";
                        }else{
                            if(!is_array($commands)){
                                $list .= "\n* /$commands";
                            }else{
                                foreach($commands as $c){
                                    $list .= "\n* /$c";
                                }
                            }
                        }
                        $chat_macro = $this->getAPI()->getPowerToolItemChatMacro($sender, $item);
                        $list .= "\n=== Chat Macro ===";
                        if($chat_macro === false){
                            $list .= "\n" . TextFormat::ITALIC . "**Предмету (" . $item->getName(). ") не назначено чат-макросов**";
                        }else{
                            $list .= "\n$chat_macro";
                        }
                        $list .= "\n=== End of the lists ===";
                        $sender->sendMessage($list);
                        return true;
                        break;
                    case "d":
                        if(!$this->getAPI()->getPowerToolItemCommand($sender, $item)){
                            $this->sendUsage($sender, $alias);
                            return false;
                        }
                        $this->getAPI()->disablePowerToolItem($sender, $item);
                        $sender->sendMessage(TextFormat::GREEN . "[✔] Команды удалены с предмета " . $item->getName(). ".");
                        return true;
                        break;
                }
            }else{
                $this->getAPI()->setPowerToolItemCommand($sender, $item, $command);
                $sender->sendMessage(TextFormat::GREEN . "[✔] Команды успешно назначены предмету в руках (" . $item->getName(). ")!");
            }
        }
        return true;
    }
} 
