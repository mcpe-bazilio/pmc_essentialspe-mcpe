<?php
namespace EssentialsPE\Commands;

use EssentialsPE\BaseFiles\BaseAPI;
use EssentialsPE\BaseFiles\BaseCommand;
use pocketmine\command\CommandSender;
use pocketmine\Player;

class Gps extends BaseCommand {
	/**
	 * @param BaseAPI $api
	 */
	public function __construct(BaseAPI $api){
		parent::__construct($api, "gps", "Ваши координаты и направление (+X: восток, +Z: юг)", "/gps", false, []);
		$this->setPermission("essentials.gps");
	}
//SSTODO объединить с getPos
	/**
	 * @param CommandSender $sender
	 * @param string        $alias
	 * @param array         $args
	 *
	 * @return bool
	 */
	public function execute(CommandSender $sender, $alias, array $args){
		if(!$this->testPermission($sender)){
			return false;
		}
		if(!$sender instanceof Player){
			$sender->sendMessage($this->getConsoleUsage());
			return false;
		}
		if(count($args) !== 0){
			$sender->sendMessage($this->getUsage());
			return false;
		}

		$r = ($sender->yaw - 90) % 360;
		if($r < 0){
			$r += 360.0;
		}
		if((0 <= $r and $r < 23) or (338 <= $r and $r < 360)){
			$d = 'север';
		}elseif(23 <= $r and $r < 68){
			$d = 'северо-восток';
		}elseif(68 <= $r and $r < 113){
			$d = 'восток';
		}elseif(113 <= $r and $r < 158){
			$d = 'юго-восток';
		}elseif(158 <= $r and $r < 203){
			$d = 'юг';
		}elseif(203 <= $r and $r < 248){
			$d = 'юго-запад';
		}elseif(248 <= $r and $r < 293){
			$d = 'запад';
		}elseif(293 <= $r and $r < 338){
			$d = 'северо-запад';
		}else{
			$d = "§c($r градусов).";
		}

		$pX = $sender->getFloorX();
		$pY = $sender->getFloorY();
		$pZ = $sender->getFloorZ();

		$info = '§eВаши координаты в мире: §b' . $sender->getLevel()->getName() . '§e: ' . $this->fCrd($pX, $pY, $pZ) . ".§e §7Направление:§6 $d";

		$sender->sendMessage($info);
		return true;
	}

	/**
	 * Форматирует координаты стандартным образом
	 *
	 * @param      $x
	 * @param      $y
	 * @param      $z
	 * @param bool $label
	 *
	 * @return string
	 */
	public function fCrd($x, $y, $z, $label = false){
		$ret = ($label ? '§dКоординаты ' : '');
		return $ret . "§7x:§6$x §7y:§6$y §7z:§6$z";
	}

}
