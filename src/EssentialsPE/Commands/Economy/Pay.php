<?php
namespace EssentialsPE\Commands\Economy;

use EssentialsPE\BaseFiles\BaseAPI;
use EssentialsPE\BaseFiles\BaseCommand;
use pocketmine\command\CommandSender;
use pocketmine\Player;
use pocketmine\utils\TextFormat;

class Pay extends BaseCommand{
    /**
     * @param BaseAPI $api
     */
    public function __construct(BaseAPI $api){
        parent::__construct($api, "pay", "Заплатить другому игроку", "<игрок> <сумма>", false);
        $this->setPermission("essentials.pay");
    }

    /**
     * @param CommandSender $sender
     * @param string $alias
     * @param array $args
     * @return bool
     */
    public function execute(CommandSender $sender, $alias, array $args): bool{
        if(!$this->testPermission($sender)){
            return false;
        }
        if(!$sender instanceof Player || count($args) !== 2){
            $this->sendUsage($sender, $alias);
            return false;
        }
        if(!($player = $this->getAPI()->getPlayer($args[0]))){
            $sender->sendMessage(TextFormat::RED . "[✘] Игрок не найден");
            return false;
        }
        if(substr($args[1], 0, 1) === "-"){
            $sender->sendMessage(TextFormat::RED . "[✘] Вы не можете заплатить отрицательную сумму");
            return false;
        }
        $balance = $this->getAPI()->getPlayerBalance($sender);
        $newBalance = $balance - (int) $args[1];
        if($balance < $args[1] || $newBalance < $this->getAPI()->getMinBalance() || ($newBalance < 0 && !$player->hasPermission("essentials.eco.loan"))){
            $sender->sendMessage(TextFormat::RED . "[✘] У вас не хватает денег");
            return false;
        }
        $sender->sendMessage(TextFormat::YELLOW . "Выплата денег...");
        $this->getAPI()->setPlayerBalance($sender, $newBalance); //Take out from the payer balance.
        $this->getAPI()->addToPlayerBalance($player, (int) $args[1]); //Pay to the other player
        return true;
    }
}