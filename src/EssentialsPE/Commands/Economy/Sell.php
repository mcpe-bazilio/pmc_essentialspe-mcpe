<?php
namespace EssentialsPE\Commands\Economy;

use EssentialsPE\BaseFiles\BaseAPI;
use EssentialsPE\BaseFiles\BaseCommand;
use pocketmine\command\CommandSender;
use pocketmine\item\Item;
use pocketmine\Player;
use pocketmine\utils\TextFormat;

class Sell extends BaseCommand{
    /**
     * @param BaseAPI $api
     */
    public function __construct(BaseAPI $api){
        parent::__construct($api, "sell", "Продать предмет или указанное количество предметов", "<item|hand> [количество]", false);
        $this->setPermission("essentials.sell");
    }

    /**
     * @param CommandSender $sender
     * @param string $alias
     * @param array $args
     * @return bool
     */
    public function execute(CommandSender $sender, $alias, array $args): bool{
        if(!$this->testPermission($sender)){
            return false;
        }
        if(!$sender instanceof Player){
            $this->sendUsage($sender, $alias);
            return false;
        }
        if($sender->getGamemode() === Player::CREATIVE || $sender->getGamemode() === Player::SPECTATOR){
            $sender->sendMessage(TextFormat::RED . "[✘] Вы в режиме " . $this->getAPI()->getServer()->getGamemodeString($sender->getGamemode()));
            return false;
        }
        if(strtolower($args[0]) === "hand"){
            $item = $sender->getInventory()->getItemInHand();
            if($item->getId() === 0){
                $sender->sendMessage(TextFormat::RED . "[✘] У вас в руках ничего нет");
                return false;
            }
        }else{
            if(!is_int($args[0])){
                $item = Item::fromString($args[0]);
            }else{
                $item = Item::get($args[0]);
            }
            if($item->getId() === 0){
                $sender->sendMessage(TextFormat::RED . "[✘] Вы указали неизвестное имя или id предмета (" . $args[0] . ")");
                return false;
            }
        }
        if(!$sender->getInventory()->contains($item)){
            $sender->sendMessage(TextFormat::RED . "[✘] У вас в инвентаре нет такого предмета (" . $item->getName() . ")");
            return false;
        }
        if(isset($args[1]) && !is_numeric($args[1])){
            $sender->sendMessage(TextFormat::RED . "[✘] Укажите правильно количество продаваемых предметов");
            return false;
        }

        $amount = $this->getAPI()->sellPlayerItem($sender, $item, (isset($args[1]) ? $args[1] : null));
        if(!$amount){
            $sender->sendMessage(TextFormat::RED . "[✘] Такой предмет продавать нельзя");
            return false;
        }elseif($amount === -1){
            $sender->sendMessage(TextFormat::RED . "[✘] У вас нет такого количества предметов (" . $args[1] . "шт)");
            return false;
        }

        if(is_array($amount)){
            $sender->sendMessage(TextFormat::GREEN . "[✔] Продано " . $amount[0] . " предметов! Вы получили " . $this->getAPI()->getCurrencySymbol() . ($amount[1] * $amount[0]));
        }else{
            $sender->sendMessage(TextFormat::GREEN . "[✔] Предмет продан! Вы получили " . $this->getAPI()->getCurrencySymbol() . $amount);
        }
        return true;
    }
}