<?php
namespace EssentialsPE\Commands\Economy;

use EssentialsPE\BaseFiles\BaseAPI;
use EssentialsPE\BaseFiles\BaseCommand;
use pocketmine\command\CommandSender;
use pocketmine\item\Item;
use pocketmine\Player;
use pocketmine\utils\TextFormat;

class SetWorth extends BaseCommand{
    /**
     * @param BaseAPI $api
     */
    public function __construct(BaseAPI $api){
        parent::__construct($api, "setworth", "Установить стоимость предмета, который у вас в руках", "<стоимость>", false);
        $this->setPermission("essentials.setworth");
    }

    /**
     * @param CommandSender $sender
     * @param string $alias
     * @param array $args
     * @return bool
     */
    public function execute(CommandSender $sender, $alias, array $args): bool{
        if(!$this->testPermission($sender)){
            return false;
        }
        if(!$sender instanceof Player || count($args) !== 1){
            $this->sendUsage($sender, $alias);
            return false;
        }elseif(!is_numeric($args[0]) || (int) $args[0] < 0){
            $sender->sendMessage(TextFormat::RED . "[✘] Укажите стоимость правильно!");
            return false;
        }elseif(($id = $sender->getInventory()->getItemInHand()->getId()) === Item::AIR){
            $sender->sendMessage(TextFormat::RED . "[✘] У вас в руках ничего нет");
            return false;
        }
        $sender->sendMessage(TextFormat::YELLOW . "Устанавливаю стоимость предмета " . Item::get($args[0])->getName() . " равную " . $this->getAPI()->getCurrencySymbol() . $args[0]);
        $this->getAPI()->setItemWorth($id, (int) $args[0]);
        return true;
    }
}