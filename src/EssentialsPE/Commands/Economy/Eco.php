<?php
namespace EssentialsPE\Commands\Economy;

use EssentialsPE\BaseFiles\BaseAPI;
use EssentialsPE\BaseFiles\BaseCommand;
use pocketmine\command\CommandSender;
use pocketmine\utils\TextFormat;

class Eco extends BaseCommand{
    /**
     * @param BaseAPI $api
     */
    public function __construct(BaseAPI $api){
        parent::__construct($api, "eco", "Изменение баланса игрока (выдать деньги | забрать деньги | установить сумму | установить начальное значение суммы)", "<give|take|set|reset> <игрок> [сумма]", true, ["economy"]);
        $this->setPermission("essentials.eco.use");
    }

    /**
     * @param CommandSender $sender
     * @param string $alias
     * @param array $args
     * @return bool
     */
    public function execute(CommandSender $sender, $alias, array $args): bool{
        if(!$this->testPermission($sender)){
            return false;
        }
        switch(count($args)){
            case 2:
            case 3:
                if(!($player = $this->getAPI()->getPlayer($args[1]))){
                    $sender->sendMessage(TextFormat::RED . "[✘] Игрок не найден");
                    return false;
                }
                if((!isset($args[2]) && strtolower($args[0]) !== "reset") || (isset($args[2]) && !is_numeric($args[2]))){
                    $sender->sendMessage(TextFormat::RED . "[✘] Укажите " . (isset($args[2]) ? " правильно" : "") . " сумму денег");
                    return false;
                }
                $balance = (int) $args[2];
                switch(strtolower($args[0])){
                    case "give":
                        $sender->sendMessage(TextFormat::YELLOW . "Добавляю деньги...");
                        $this->getAPI()->addToPlayerBalance($player, $balance);
                        break;
                    case "take":
                        $sender->sendMessage(TextFormat::YELLOW . "Забираю деньги...");
                        $this->getAPI()->addToPlayerBalance($player, -$balance);
                        break;
                    case "set":
                        $sender->sendMessage(TextFormat::YELLOW . "Устанавливаю сумму денег...");
                        $this->getAPI()->setPlayerBalance($player, $balance);
                        break;
                    case "reset":
                        $sender->sendMessage(TextFormat::YELLOW . "Переустанавливаю сумму денег...");
                        $this->getAPI()->setPlayerBalance($player, $this->getAPI()->getDefaultBalance());
                        break;
                }
                break;
            default:
                $this->sendUsage($sender, $alias);
                break;
        }
        return true;
    }
}