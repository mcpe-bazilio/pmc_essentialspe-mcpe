<?php
namespace EssentialsPE\Commands;

use EssentialsPE\BaseFiles\BaseAPI;
use EssentialsPE\BaseFiles\BaseCommand;
use pocketmine\command\CommandSender;
use pocketmine\Player;
use pocketmine\utils\TextFormat;

class Compass extends BaseCommand {
	/**
	 * @param BaseAPI $api
	 */
	public function __construct(BaseAPI $api){
		parent::__construct($api, "compass", "Покажет ваше напровление по компасу", null, false, ["direction"]);
		$this->setPermission("essentials.compass");
	}

	/**
	 * @param CommandSender $sender
	 * @param string        $alias
	 * @param array         $args
	 *
	 * @return bool
	 */
	public function execute(CommandSender $sender, $alias, array $args): bool{
		if(!$this->testPermission($sender)){
			return false;
		}
		if(!$sender instanceof Player || count($args) !== 0){
			$this->sendUsage($sender, $alias);
			return false;
		}
		switch($sender->getDirection()){
			case 0:
				$direction = "south";
				break;
			case 1:
				$direction = "west";
				break;
			case 2:
				$direction = "north";
				break;
			case 3:
				$direction = "east";
				break;
			default:
				$sender->sendMessage(TextFormat::RED . "[✘] Произошла ошибка при получении вашего направления");
				return false;
				break;
		}
		$sender->sendMessage(TextFormat::AQUA . "[✔] Ваше направление: " . TextFormat::YELLOW . $direction);
		return true;
	}
}
