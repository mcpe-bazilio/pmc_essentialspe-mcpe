<? //(>sendMessage|__construct)\(.+$

//	Hat.php  
		/*16*/	parent::__construct($api, "hat", "Get some new cool headgear", "[remove]", false, ["head"]);
		/*43*/	$sender->sendMessage(TextFormat::RED . "[✘] Please specify an item to wear");
		/*52*/	$sender->sendMessage(TextFormat::AQUA . ($new->getId() === Item::AIR ? "Hat removed!" : "You got a new hat!"));
//	Heal.php  
		/*17*/	parent::__construct($api, "heal", "Heal yourself or other player", "[игрок]");
		/*42*/	$player->sendMessage(TextFormat::GREEN . "[✔] You have been healed!");
		/*44*/	$sender->sendMessage(TextFormat::GREEN . $player->getDisplayName() . " has been healed!");
//	ItemCommand.php  
		/*16*/	parent::__construct($api, "item", "Gives yourself an item", "<item[:damage]> [amount]", false, ["i"]);
		/*35*/	$sender->sendMessage(TextFormat::RED . "[✘] You're in " . $this->getAPI()->getServer()->getGamemodeString($gm) . " mode");
		/*43*/	$sender->sendMessage(TextFormat::RED . "Unknown item \"" . $item_name . "\"");
		/*46*/	$sender->sendMessage(TextFormat::RED . "You can't spawn this item");
		/*65*/	->sendMessage(TextFormat::YELLOW . "Giving " . TextFormat::RED . $item->getCount() . TextFormat::YELLOW . " of " . TextFormat::RED . ($item->getName() === "Unknown" ? $item_name : $item->getName()));
//	ItemDB.php  
		/*15*/	parent::__construct($api, "itemdb", "Display the information attached to the item you hold", "[name|id|meta]", false, ["itemno", "durability", "dura"]);
		/*51*/	$sender->sendMessage($m);
//	Jump.php  
		/*15*/	parent::__construct($api, "jump", "Teleport you to the block you're looking at", null, false, ["j", "jumpto"]);
		/*35*/	$sender->sendMessage(TextFormat::RED . "There isn't a reachable block");
//	KickAll.php  
		/*15*/	parent::__construct($api, "kickall", "Kick all the players", "<reason>");
		/*30*/	$sender->sendMessage(TextFormat::RED . "[✘] There are no more players in the server");
		/*43*/	$sender->sendMessage(TextFormat::AQUA . "Kicked all the players!");
//	Kit.php  
		/*15*/	parent::__construct($api, "kit", "Get a pre-defined kit!", "[name] [игрок]", "[<name> <игрок>]", ["kits"]);
		/*34*/	$sender->sendMessage(TextFormat::AQUA . "There are currently no Kits available");
		/*37*/	$sender->sendMessage(TextFormat::AQUA . "Available kits:\n" . $list);
		/*43*/	$sender->sendMessage(TextFormat::RED . "[✘] Kit doesn't exist");
		/*53*/	$sender->sendMessage(TextFormat::RED . "[✘] You can't obtain this kit");
		/*57*/	$sender->sendMessage(TextFormat::GREEN . "[✔] Getting kit " . TextFormat::AQUA . $kit->getName() . "...");
		/*65*/	$sender->sendMessage(TextFormat::RED . "[✘] You can't obtain this kit");
		/*73*/	$player->sendMessage(TextFormat::GREEN . "[✔] Getting kit " . TextFormat::AQUA . $kit->getName() . "...");
		/*74*/	->sendMessage(TextFormat::GREEN . "[✔] Giving " . TextFormat::YELLOW . $player->getDisplayName() . TextFormat::GREEN . " kit " . TextFormat::AQUA . $kit->getName() . TextFormat::GREEN . "...");
		/*92*/	$sender->sendMessage(TextFormat::RED . "[✘] You can't obtain this kit");
		/*95*/	$player->sendMessage(TextFormat::GREEN . "[✔] Getting kit " . TextFormat::AQUA . $kit->getName() . "...");
		/*97*/	->sendMessage(TextFormat::GREEN . "[✔] Giving " . TextFormat::YELLOW . $player->getDisplayName() . TextFormat::GREEN . " kit " . TextFormat::AQUA . $kit->getName() . TextFormat::GREEN . "...");
//	Lightning.php  
		/*15*/	parent::__construct($api, "lightning", "Strike lightning", "[player [damage]]", "<игрок> [damage]", ["strike", "smite", "thor", "shock"]);
		/*41*/	$sender->sendMessage(TextFormat::YELLOW . "Lightning launched!");
//	More.php  
		/*16*/	parent::__construct($api, "more", "Get a stack of the item you're holding", null, false);
		/*35*/	$sender->sendMessage(TextFormat::RED . "[✘] You're in " . $this->getAPI()->getServer()->getGamemodeString($gm) . " mode");
		/*40*/	$sender->sendMessage(TextFormat::RED . "You can't get a stack of AIR");
		/*44*/	$sender->sendMessage(TextFormat::AQUA . "Filled up the item stack to " . $item->getCount());
//	Mute.php  
		/*15*/	parent::__construct($api, "mute", "Prevent a player from chatting", "<игрок> [time...]", true, ["silence"]);
		/*38*/	$sender->sendMessage(TextFormat::RED . $player->getDisplayName() . " can't be muted");
		/*47*/	->sendMessage(TextFormat::YELLOW . $player->getDisplayName() . " has been " . ($this->getAPI()->isMuted($player) ? "muted " . ($date !== null ? "until: " . TextFormat::AQUA . $date->format("l, F j, Y") . TextFormat::RED . " at " . TextFormat::AQUA . $date->format("h:ia") : TextFormat::AQUA . "Forever" . TextFormat::YELLOW . "!") : "unmuted!"));
//	Near.php  
		/*15*/	parent::__construct($api, "near", "List the players near to you", "[игрок]", true, ["nearby"]);
		/*52*/	$sender->sendMessage($m);
//	Nick.php  
		/*15*/	parent::__construct($api, "nick", "Change your in-game name", "<new nick|off> [игрок]", true, ["nickname"]);
		/*48*/	$sender->sendMessage(TextFormat::RED . "[✘] You don't have permissions to give 'colored' nicknames");
		/*51*/	$player->sendMessage(TextFormat::GREEN . "[✔] Your nick " . ($m = !$nick ? "has been removed" : "is now " . TextFormat::RESET . $nick));
		/*53*/	$sender->sendMessage(TextFormat::GREEN . $player->getName() . (substr($player->getName(), -1, 1) === "s" ? "'" : "'s") . " nick " . $m);
//	Nuke.php  
		/*15*/	parent::__construct($api, "nuke", "Lay down a carpet of TNT", "[игрок]");
//	Ping.php  
		/*14*/	parent::__construct($api, "ping", "Pong!");
		/*27*/	$sender->sendMessage("Pong!");
//	PTime.php  
		/*16*/	parent::__construct($api, "ptime", "Changes the time of a player", "<time> [игрок]", true, ["playertime"]);
		/*68*/	$sender->sendMessage(TextFormat::RED . "Something went wrong while setting the time");
		/*71*/	$sender->sendMessage(TextFormat::GREEN . "[✔] Setting player time...");
//	PvP.php  
		/*15*/	parent::__construct($api, "pvp", "Toggle PvP on/off", "<on|true|enable|off|false|disable>", false);
		/*34*/	$sender->sendMessage(TextFormat::GREEN . "[✔] PvP mode " . ($s ? "enabled" : "disabled"));
//	RealName.php  
		/*14*/	parent::__construct($api, "realname", "Check the real name of a player", "<игрок>");
		/*36*/	$sender->sendMessage(TextFormat::YELLOW .  $player->getDisplayName() . (substr($player->getName(), -1, 1) === "s" ? "'" : "'s") . " realname is: " . TextFormat::RED . $player->getName());
//	Repair.php  
		/*15*/	parent::__construct($api, "repair", "Repair items in your inventory", "[all|hand]", false, ["fix"]);
		/*61*/	$sender->sendMessage(TextFormat::RED . "[✘] This item can't be repaired!");
		/*67*/	$sender->sendMessage($m);
//	Reply.php  
		/*17*/	parent::__construct($api, "reply", "Quickly reply to the last person that messaged you", "<message ...>", true, ["r"]);
		/*36*/	$sender->sendMessage(TextFormat::RED . "[✘] No target available for QuickReply");
		/*41*/	$sender->sendMessage(TextFormat::RED . "[✘] No player available for QuickReply");
		/*46*/	$sender->sendMessage(TextFormat::YELLOW . "[me -> " . ($t instanceof Player ? $t->getDisplayName() : $t) . "]" . TextFormat::RESET . " " . implode(" ", $args));
		/*49*/	$t->sendMessage($m);
//	Seen.php  
		/*15*/	parent::__construct($api, "seen", "See player's last played time", "<игрок>");
		/*34*/	$sender->sendMessage(TextFormat::GREEN . $player->getDisplayName() . " is online!");
		/*38*/	$sender->sendMessage(TextFormat::RED .  $args[0] . " has never played on this server.");
		/*50*/	->sendMessage(TextFormat::AQUA .  $player->getName() ." was last seen on " . TextFormat::RED . date("l, F j, Y", ($t = $player->getLastPlayed() / 1000)) . TextFormat::AQUA . " at " . TextFormat::RED . date("h:ia", $t));
//	Spawn.php  
		/*16*/	parent::__construct($api, "spawn", "Teleport to server's main spawn", "[игрок]");
		/*37*/	$sender->sendMessage(TextFormat::RED . "[✘] You can't teleport other players to spawn");
		/*45*/	$player->sendMessage(TextFormat::GREEN . "[✔] Teleporting...");
//	Speed.php  
		/*13*/	parent::__construct($api, "speed", "Change your speed limit", "<speed> [игрок]");
		/*26*/	$sender->sendMessage(TextFormat::RED . "[✘] Please provide a valid value");
		/*42*/	$sender->sendMessage(TextFormat::YELLOW . "Speed amplified by " . TextFormat::WHITE . $args[0]);
//	Sudo.php  
		/*15*/	parent::__construct($api, "sudo", "Run a command as another player", "<игрок> <command line|c:<chat message>");
		/*37*/	$sender->sendMessage(TextFormat::RED . "[✘] " . $player->getName() . " cannot be sudo'ed");
		/*43*/	$sender->sendMessage(TextFormat::GREEN . "[✔] Sending message as " .  $player->getDisplayName());
		/*49*/	$sender->sendMessage(TextFormat::AQUA . "Command ran as " .  $player->getDisplayName());
//	Suicide.php  
		/*15*/	parent::__construct($api, "suicide", "Kill yourself", null, false);
		/*39*/	$sender->sendMessage("Ouch. That looks like it hurt.");
//	TempBan.php  
		/*15*/	parent::__construct($api, "tempban", "Temporarily bans the specified player", "<игрок> <time...> [reason ...]");
		/*35*/	$sender->sendMessage(TextFormat::RED . "[✘] Please specify a valid time");
		/*43*/	$sender->sendMessage(TextFormat::RED . "[✘] " . $player->getDisplayName() . " can't be banned");
//	Top.php  
		/*16*/	parent::__construct($api, "top", "Teleport to the highest block above you", null, false);
		/*34*/	$sender->sendMessage(TextFormat::YELLOW . "Teleporting...");
//	TreeCommand.php  
		/*18*/	parent::__construct($api, "tree", "Spawns a tree", "<tree|birch|redwood|jungle>", false);
		/*38*/	$sender->sendMessage(TextFormat::RED . "There isn't a reachable block");
//	Unlimited.php  
		/*15*/	parent::__construct($api, "unlimited", "Allow you to place unlimited blocks", "[игрок]", true, ["ul", "unl"]);
		/*44*/	->sendMessage(TextFormat::RED . "[✘] " . ($player === $sender ? "you are" : $player->getDisplayName() . " is") . " in " . $this->getAPI()->getServer()->getGamemodeString($gm) . " mode");
		/*48*/	$player->sendMessage(TextFormat::GREEN . "[✔] Unlimited placing of blocks " . ($s = $this->getAPI()->isUnlimitedEnabled($player) ? "enabled" : "disabled"));
		/*50*/	$sender->sendMessage(TextFormat::GREEN . "[✔] Unlimited placing of blocks $s");
//	Vanish.php  
		/*15*/	parent::__construct($api, "vanish", "Hide from other players", "[игрок]", true, ["v"]);
		/*36*/	$sender->sendMessage($this->getPermissionMessage());
		/*44*/	$player->sendMessage(TextFormat::GRAY . "You're now " . ($s = $this->getAPI()->isVanished($player) ? "vanished" : "visible"));
		/*46*/	$sender->sendMessage(TextFormat::GRAY .  $player->getDisplayName() . " is now $s");
//	Whois.php  
		/*14*/	parent::__construct($api, "whois", "Display player information", "<игрок>");
		/*44*/	$sender->sendMessage($m);
//	World.php  
		/*15*/	parent::__construct($api, "world", "Teleport between worlds", "<world name>", false);
		/*34*/	$sender->sendMessage(TextFormat::RED . "[✘] You can't teleport to this world.");
		/*38*/	$sender->sendMessage(TextFormat::RED . "[✘] World doesn't exist");
		/*41*/	$sender->sendMessage(TextFormat::YELLOW . "Level is not loaded yet. Loading...");
		/*43*/	$sender->sendMessage(TextFormat::RED . "[✘] The level couldn't be loaded");
		/*48*/	$sender->sendMessage(TextFormat::YELLOW . "Teleporting...");
